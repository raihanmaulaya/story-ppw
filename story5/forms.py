from django import forms
from . models import PostMalon

# class PostForm(forms.Form):
# 	# judul = forms.CharField(max_length=100)
# 	# body = forms.CharField(widget=forms.Textarea)
# 	# category = forms.CharField(max_length=100)


class PostForm(forms.ModelForm):
	class Meta:
		model = PostMalon
		fields = [
			'nama_mata_kuliah',
			'dosen_pengajar',
			'jumlah_sks',
			'deskripsi_mata_kuliah',
			'semester_tahun',
			'ruang_kelas',
		]