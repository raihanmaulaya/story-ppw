
from django.urls import path 
from . import views

app_name = 'story5'
urlpatterns = [
	path('delete/<str:delete_id>', views.delete, name='delete'),
	path('update/<str:update_id>', views.update, name='update'),
	path('create/', views.create, name='create'),
    path('', views.index, name='index'),

]