from django.db import models

# Create your models here.
class PostMalon(models.Model):
	nama_mata_kuliah =  models.CharField(max_length = 100, )
	dosen_pengajar = models.CharField(max_length = 100, )
	jumlah_sks = models.IntegerField()
	deskripsi_mata_kuliah = models.CharField(max_length = 100,)

	LIST_TAHUN = (
		('Genap 2020/2021', 'Genap 2020/2021'),
		('Gasal 2020/2021', 'Gasal 2020/2021'),
		('Genap 2019/2020', 'Genap 2019/2020'),
		('Gasal 2019/2020', 'Gasal 2019/2020'),
	)
	semester_tahun = models.CharField (
		max_length = 100,	
		choices = LIST_TAHUN,
	)

	ruang_kelas = models.CharField(max_length = 100,)


	def __str__(self):
		return "{}.{}".format(self.id,self.nama_mata_kuliah)
