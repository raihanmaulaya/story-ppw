from django.shortcuts import render, redirect

# Create your views here.
from .forms import PostForm
from .models import PostMalon 


def update(request,update_id):
	list_update = PostMalon.objects.get(id=update_id)
	data = {
		'nama_mata_kuliah' : list_update.nama_mata_kuliah,
		'dosen_pengajar'	: list_update.dosen_pengajar,
		'jumlah_sks'		: list_update.jumlah_sks,
		'deskripsi_mata_kuliah'	: list_update.deskripsi_mata_kuliah,
		'semester_tahun'	: list_update.semester_tahun,
		'ruang_kelas'		:list_update.ruang_kelas,
}

	post_form = PostForm(request.POST or None, initial=data, instance=list_update)
	if request.method == 'POST':
		if post_form.is_valid():
			post_form.save()

			return redirect('story5:index')
			
	context = {
	'page_title': 'Story 5 Jace',
	'post_title' : 'Update Post',
	'post_form'	 : post_form,
	'desc'	: 'Untuk mengupdate mata kuliah',
	}		

	return render(request,'create.html', context)

def delete(request,delete_id):
	mendapatkan = PostMalon.objects.get(id=delete_id)
	if request.method == "POST":
		mendapatkan.delete()
		return redirect('story5:index')

def index(request):
	posts = PostMalon.objects.all()

	context = {
		'page_title':'Story 5 Jace',
		'posts': posts,
		'post_title': 'Mata Kuliahku',
		'desc'	: 'Ini adalah Mata Kuliah dari Jace!'
	}
	
	return render(request,'add.html', context)


def create(request):
	post_form = PostForm(request.POST or None)
	if request.method == 'POST':
		if post_form.is_valid():
			post_form.save()
	
		
		return redirect('story5:index')

	context = {
		'page_title': 'Story 5 Jace',
		'post_title' : 'Create Post',
		'post_form'	 : post_form,
		'desc'	: 'Untuk menambah mata kuliah'
	}
	return render(request,'create.html', context)
