from django.urls import path 
from . import views

app_name = 'story3ku'
urlpatterns = [
    path('', views.landing, name='landing'),
    path('story3/', views.home, name='home'),
]

