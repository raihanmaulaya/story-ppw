from django.test import TestCase, Client
from django.urls import resolve
from .views import index, library
from .apps import Story8Config

# Create your tests here.


class TestStoryDelapanIndex(TestCase):
    def test_event_url_is_exist(self):
        response = Client().get('/story8/')
        self.assertEqual(response.status_code, 200)

    def test_event_index_func(self):
        found = resolve('/story8/')
        self.assertEqual(found.func, index)

    def test_event_using_template(self):
        response = Client().get('/story8/')
        self.assertTemplateUsed(response, 'story-8.html')


class TestStoryDelapanLibrary(TestCase):
    def test_event_url_is_exist(self):
        response = Client().get('/story8/isibuku/?q=Jace')
        self.assertEqual(response.status_code, 200 )

    def test_event_text_is_exist(self):
        response = Client().get('/story8/isibuku/?q=Jace')
        text = response.content.decode('utf8')
        self.assertIn("Jace", text)

class TestApp(TestCase):
    def test_app(self):
        self.assertEqual(Story8Config.name, 'story8')
