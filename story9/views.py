from django.shortcuts import render

# Create your views here.
from django.shortcuts import render, redirect
from django.urls import reverse_lazy
from django.views.generic import CreateView
from . import forms
from django.views.generic import TemplateView

# Create your views here.

class HomePage(TemplateView):
	template_name = "indexTK.html"


class SignUp(CreateView):
	form_class = forms.UserCreateForm
	success_url = reverse_lazy('login')
	template_name = 'signup.html'	


class TestPage(TemplateView):
	template_name = "test.html"


class ThanksPage(TemplateView):
	template_name = "thanks.html"





