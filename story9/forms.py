from django.contrib.auth import get_user_model
from django.contrib.auth.forms import UserCreationForm
from django import forms
from django.contrib.auth.models import User

class UserCreateForm(UserCreationForm):
	first_name=forms.CharField(max_length=100, help_text="First Name")
	last_name=forms.CharField(max_length=100, help_text="Last Name")

	class Meta:
		fields = ('username', 'first_name','last_name','email', 'password1','password2')
		model = User


	def __init__(self,*args,**kwargs):
		super().__init__(*args,**kwargs)
		self.fields['username'].label = 'Display Name'
		self.fields['email'].label = 'Email Address'


		 

