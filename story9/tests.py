from django.test import TestCase, Client
from django.contrib.auth import authenticate, get_user_model
from .views import SignUp 
from .apps import Story9Config
# Create your tests here.
class SigninTest(TestCase):

    def setUp(self):
        self.user = get_user_model().objects.create_user(username='test', password='12test12', email='test@example.com')
        self.user.save()

    def tearDown(self):
        self.user.delete()

    def test_correct(self):
        user = authenticate(username='test', password='12test12')
        self.assertTrue((user is not None) and user.is_authenticated)

    def test_wrong_username(self):
        user = authenticate(username='wrong', password='12test12')
        self.assertFalse(user is not None and user.is_authenticated)

    def test_wrong_pssword(self):
        user = authenticate(username='test', password='wrong')
        self.assertFalse(user is not None and user.is_authenticated)

class TestJace(TestCase):
    def test_appdjangu_name(self):
        self.assertEqual(Story9Config.name, "story9")

    def test_url_is_exist_login(self):
        response = Client().get('/story9/') 
        self.assertEqual(response.status_code, 200)

    def test_url_is_exist_signup(self):
        response = Client().get('/story9/signup/') 
        self.assertEqual(response.status_code, 200)


    def test_using_template_login(self):
        response = Client().get('/story9/')
        self.assertTemplateUsed(response, 'login.html')

    def test_using_template_signup(self):
        response = Client().get('/story9/signup/')
        self.assertTemplateUsed(response, 'signup.html')