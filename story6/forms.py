from django import forms
from . models import PostKegiatan,PostPeserta

class KegiatanForm(forms.ModelForm):
    class Meta:
        model = PostKegiatan
        fields = [
            'kegiatan',
        ]

class PesertaForm(forms.ModelForm):
    class Meta:
        model = PostPeserta
        fields = [
            'peserta',
        ]