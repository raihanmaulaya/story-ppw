from django.test import TestCase, Client
from django.urls import resolve
from .views import index, createKegiatan,createPeserta
from .models import PostPeserta, PostKegiatan 
from .apps import Story6Config

# Create your tests here.
class TestKegiatan(TestCase):
    def test_url_is_exist(self):
        response = Client().get('/story6/') 
        self.assertEqual(response.status_code, 200)

    def test_index_func(self):
        found =  resolve('/story6/')
        self.assertEqual(found.func, index)

    def test_using_template(self):
        response = Client().get('/story6/')
        self.assertTemplateUsed(response, 'story6.html')


class TestTambahKegiatan(TestCase):
    def test_create_url_is_exist(self):
        response = Client().get('/story6/create/')
        self.assertEqual(response.status_code, 200)

    def test_create_func(self):
        found = resolve('/story6/create/')
        self.assertEqual(found.func, createKegiatan)

    def test_create_using_template(self):
        response = Client().get('/story6/create/')
        self.assertTemplateUsed(response, 'create6.html')

    def test_create_new_kegiatan(self):
        kegiatan = PostKegiatan(kegiatan = "PPW")
        kegiatan.save()
        self.assertEqual(PostKegiatan.objects.all().count(),1)

    def test_url_kegiatan_is_exist(self):
        response = Client().post('/story6/', data={'kegiatan':'Coding'})
        self.assertEqual(response.status_code, 200)

class TestPeserta(TestCase) :
    def setUp(self):
        kegiatan = PostKegiatan(kegiatan="PPW")
        kegiatan.save()
        self.assertEqual(PostKegiatan.objects.all().count(),1)

    def test_regris_POST(self):
        response = Client().post('/story6/createPeserta/1/', data={'peserta':'Jace'})
        self.assertEqual(response.status_code, 302)

    def test_regris_GET(self):
        response = self.client.get('/story6/createPeserta/1/')
        self.assertTemplateUsed(response, 'createOrang.html')
        self.assertEqual(response.status_code, 200)

class TestHapusNama(TestCase):
    def setUp(self):
        acara = PostKegiatan(kegiatan="Coding")
        acara.save()
        nama = PostPeserta(peserta="Jace")
        nama.save()

    def test_hapus_url_post_is_exist(self):
        response = Client().post('/story6/deleteOrang/1/')
        self.assertEqual(response.status_code, 302)

    def test_hapus_url_is_exist(self):
        response = self.client.get('/story6/deleteOrang/1/')
        self.assertEqual(response.status_code, 302)


class TestHapusKegiatan(TestCase):
    def setUp(self):
        acara = PostKegiatan(kegiatan="Coding")
        acara.save()
        nama = PostPeserta(peserta="Jace")
        nama.save()

    def test_hapus_url_activity_is_exist(self):
        response = Client().post('/story6/delete/1/')
        self.assertEqual(response.status_code, 302)

    def test_hapus_activity_url_is_exist(self):
        response = self.client.get('/story6/delete/1/')
        self.assertEqual(response.status_code, 302)

         
class TestApp(TestCase):
    def test_app(self):
        self.assertEqual(Story6Config.name, 'story6')












