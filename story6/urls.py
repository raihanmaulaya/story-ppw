from django.urls import path 
from . import views

app_name = 'story6'
urlpatterns = [
	path('delete/<int:id_kegiatan>/', views.delete, name='delete'),
	path('deleteOrang/<int:id_peserta>/', views.deleteOrang, name='deleteOrang'),
	path('create/', views.createKegiatan, name='createActivity'),
	path('createPeserta/<int:id_orang>/', views.createPeserta, name='createPeserta'),
    path('', views.index, name='index'),

]