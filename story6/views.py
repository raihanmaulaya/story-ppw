from django.shortcuts import render,redirect

# Create your views here.
from .forms import KegiatanForm, PesertaForm
from .models import PostKegiatan, PostPeserta

def index(request):
    kegiatan = PostKegiatan.objects.all()
    peserta = PostPeserta.objects.all()

    context = {
        'page_title':'Story 6 Jace',
		'kegiatan': kegiatan,
        'peserta': peserta,
		'post_title': 'Kegiatan Saya dan Teman-teman',
		'desc'	: 'HAPPY ANNIVERSARY CDG!'
    }

    return render(request,'story6.html', context)

def createKegiatan(request):
    formkegiatan = KegiatanForm(request.POST)
    if request.method == "POST":
        if formkegiatan.is_valid():
            formkegiatan.save()
            return redirect ('/story6')


        return redirect('story6:index')

    context = {
         'page_title':'Story 6 Jace',
		'post_title': 'Tambahkan Kegiatan',
		'desc'	: 'HAPPY ANNIVERSARY CDG!',
        'formkegiatan' : formkegiatan,
    }

    return render(request,'create6.html', context)

def createPeserta(request, id_orang):
    formpeserta = PesertaForm(request.POST)
    if request.method == "POST":
        if formpeserta.is_valid():
            peserta = PostPeserta(ikutKegiatan = PostKegiatan.objects.get(id=id_orang),
                peserta=formpeserta.data['peserta'])
            peserta.save()    

        return redirect('story6:index')

    context = {
        'form':formpeserta
    }

    return render(request,'createOrang.html', context)

def delete(request, id_kegiatan):
    PostKegiatan.objects.filter(id=id_kegiatan).delete()
    return redirect('story6:index')


def deleteOrang(request, id_peserta):
    PostPeserta.objects.filter(id=id_peserta).delete()
    return redirect('story6:index')