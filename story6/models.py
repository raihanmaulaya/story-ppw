from django.db import models


class PostKegiatan(models.Model):
    kegiatan = models.CharField(max_length=100)

class PostPeserta(models.Model):
    peserta = models.CharField(max_length=100)
    ikutKegiatan = models.ForeignKey(
        PostKegiatan, on_delete=models.CASCADE, null=True, blank=True)
# Create your models here.
