from django.test import TestCase, Client
from django.urls import resolve
from .views import index
from .apps import Story7Config

# Create your tests here.
class TestKegiatan(TestCase):
	def test_template(self):
		response = Client().get('/story7/')
		self.assertTemplateUsed(response, 'story-7.html')

	def test_url_is_exist(self):
		response = Client().get('/story7/') 
		self.assertEqual(response.status_code, 200)

	def test_index_func(self):
		found =  resolve('/story7/')
		self.assertEqual(found.func, index)

class TestApp(TestCase):
    def test_app(self):
        self.assertEqual(Story7Config.name, 'story7' )